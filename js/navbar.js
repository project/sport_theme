const navbar = ($) => {
  $(document).ready(function () {
    $('.hamburger').click(function () {
      $(this).toggleClass('hamclick');
      $('.navbar .menu').toggle(300);
    });
  });
};
navbar(jQuery);